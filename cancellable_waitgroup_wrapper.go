// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// Alfredo - a compact framework for concurrency / thread programming in golang.
// Copyright (C) 2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package alfredo

import (
	"fmt"
	"sync"

	y "gitlab.com/quoeamaster/yalf"
)

// CancellableWaitgroup - a goroutine and channel framework, waitgroup is included in this version;
// hence no runnning / pending tasks should be ignored like [RoutineChannelCancellable] DOES.
type CancellableWaitgroup struct {
	// taskChannel - an object containing info for operation.
	taskChannel chan interface{}
	// signalChannel - a signal channel, usually accept the Cancel signal
	signalChannel chan int
	// delegate - instance of the operation delegate.
	delegate CancellableWaitgroupDelegate

	// lock - a mutex lock for thread safe access on member variables.
	lock sync.Mutex
	// wg - waitgroup for waiting the in-process task(s) to finish before exiting.
	wg sync.WaitGroup
	// cancel - a flag to indicate whether a stop-signal has been received.
	cancel bool

	logger y.IStream
}

// CancellableWaitgroupDelegate - delegate for the [CancellableWaitgroup].
type CancellableWaitgroupDelegate interface {
	Execute(meta interface{}) (e error)
}

// NewCancellableWaitgroup - create the instance of [CancellableWaitgroup].
func NewCancellableWaitgroup(delegate CancellableWaitgroupDelegate) (o *CancellableWaitgroup, e error) {
	o = new(CancellableWaitgroup)
	o.taskChannel = make(chan interface{})
	o.signalChannel = make(chan int)
	o.delegate = delegate
	if delegate == nil {
		e = fmt.Errorf("[concurrency][NewCancellableWaitgroup] missing a valid delegate")
	}
	o.lock = sync.Mutex{}
	o.wg = sync.WaitGroup{}

	o.logger = y.NewLogFactory().Config(nil).GetStream(y.StreamStdOut)

	return
}

func (o *CancellableWaitgroup) log(msg string, logLevel string, funcName string) {
	o.logger.LogByModel(y.LogModel{
		Message:       msg,
		Level:         logLevel,
		FuncName:      fmt.Sprintf("concurrency.%v", funcName),
		NeedTimestamp: true,
	})
}

// Push - push a task [meta] into the task-channel.
func (o *CancellableWaitgroup) Push(meta interface{}) {
	o.log(fmt.Sprintf(" %v -> isCancel [%v]", meta, o.cancel), y.LogLevelDebug, "push")
	if meta != nil && !o.cancel {
		o.log(fmt.Sprintf(" %v JUST before unlock", meta), y.LogLevelDebug, "push")
		o.lock.Lock()
		o.taskChannel <- meta
		o.lock.Unlock()
		o.log(fmt.Sprintf(" %v AFTER before unlock", meta), y.LogLevelDebug, "push")
		o.log(fmt.Sprintf(" pushed~ %v\n", meta), y.LogLevelDebug, "push")
	}
}

// Run
func (o *CancellableWaitgroup) Run() {
	go func() {
		_loop := true
		for _loop {
			select {
			case _signal := <-o.signalChannel:
				if _signal == SignalStop {
					o.lock.Lock()
					defer o.lock.Unlock()

					o.log(" stop signal recived...", y.LogLevelInfo, "Run")
					o.cancel = true
					close(o.signalChannel)
					// [fact?] - might have issue if closing the channel too fast...
					close(o.taskChannel)
					// break the loop
					_loop = false
				}
			case _meta := <-o.taskChannel:
				// update the waitgroup
				o.wg.Add(1)
				// run in parallel (hence another goroutine here, task by task)
				go func() {
					o.log(fmt.Sprintf(" start handling %v", _meta), y.LogLevelInfo, "Run")
					if e := o.delegate.Execute(_meta); e != nil {
						// let the caller to handle the error
						panic(e)
					} else {
						o.wg.Done()
					}
				}()
			default:
				// [bug?] - if the "default" is not applied... would not work (why???)
				//fmt.Printf(".")
			}
		}
		// [lesson] - if wait() is called here, all tasks would be stopped at once since the execution has not yet even started...
		// the waitgroup.Wait() should be called by the caller at a "suitable" moment and hence there is no auto Wait applied here
	}()
}

// Cancel - stop the tasks for running; existing / in-process task would still be operated but no more new task should be addable.
func (o *CancellableWaitgroup) Cancel() {
	o.signalChannel <- SignalStop
}

func (o *CancellableWaitgroup) Wait() {
	// blocking until all task done
	o.wg.Wait()
	o.log(" wait done...", y.LogLevelDebug, "Wait")
}
