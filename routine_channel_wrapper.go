// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// Alfredo - a compact framework for concurrency / thread programming in golang.
// Copyright (C) 2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package alfredo

import (
	"fmt"
	"sync"

	y "gitlab.com/quoeamaster/yalf"
)

// RoutineChannelCancellableDelegate - the delegate / function for execution when a [meta] is available for processing.
type RoutineChannelCancellableDelegate interface {
	// Execute - contains the biz logic for the provided [meta].
	Execute(meta interface{}) (e error)
}

type RoutineChannelCancellable struct {
	taskChannel   chan interface{}
	signalChannel chan int
	delegate      RoutineChannelCancellableDelegate

	// cancelled - status indicating the tasks should be stopped
	cancelled bool
	lock      sync.Mutex

	logger y.IStream
}

func NewRoutineChannelCancellable(capacity int, delegate RoutineChannelCancellableDelegate) (o *RoutineChannelCancellable, e error) {
	o = new(RoutineChannelCancellable)
	if capacity > 0 {
		// efficient but restricted to the [capacity] number of routines at MAX
		o.taskChannel = make(chan interface{}, capacity)
	} else {
		// unbuffered, less efficient but more flexible
		o.taskChannel = make(chan interface{})
	}
	// unbuffered as possible to received numerous signals before terminating the program / process.
	o.signalChannel = make(chan int)
	// delegate to run when tasks are available...
	o.delegate = delegate
	if o.delegate == nil {
		e = fmt.Errorf("[concurrency][NewRoutineChannelCancellable] missing [delegate] for execution")
	}

	// setup the mutex
	o.cancelled = false
	o.lock = sync.Mutex{}

	o.logger = y.NewLogFactory().Config(nil).GetStream(y.StreamStdOut)

	return
}

func (o *RoutineChannelCancellable) log(msg string, logLevel string, funcName string) {
	o.logger.LogByModel(y.LogModel{
		Message:       msg,
		Level:         logLevel,
		FuncName:      fmt.Sprintf("concurrency.%v", funcName),
		NeedTimestamp: true,
	})
}

// Push - add a new [meta] or task to the channel. The receiver side would extract it for biz logic processing.
func (o *RoutineChannelCancellable) Push(meta interface{}) {
	o.lock.Lock()
	defer o.lock.Unlock()

	if meta != nil && !o.cancelled {
		o.taskChannel <- meta
		o.log(fmt.Sprintf(" task pushed [%v]", meta), y.LogLevelDebug, "Push")
	}
}

// Run - run an endless loop to retrieve a task and execute it through the [delegate] configured.
func (o *RoutineChannelCancellable) Run() {
	go func() {
		for {
			select {
			case _meta := <-o.taskChannel:
				if e := o.delegate.Execute(_meta); e != nil {
					// the caller should recover() the error; since within a go-routine, no way to pass back the error
					o.log(fmt.Sprintf(" error on Execution, reason: %v", e), y.LogLevelWarn, "Run")
					panic(e)
				}
			case _signal := <-o.signalChannel:
				switch _signal {
				case SignalStop:
					o.lock.Lock()
					defer o.lock.Unlock()

					o.cancelled = true
					close(o.signalChannel)
					close(o.taskChannel)
					return
				}
			}
		}
	}()
}

// Cancel - send a stop signal to the signals channel.
func (o *RoutineChannelCancellable) Cancel() {
	// can apply the mutex locking; though not likely since usually only change the signal once... [StopSignal]
	o.signalChannel <- SignalStop
	o.log(" stop signal sent", y.LogLevelWarn, "Cancel")
}
