module gitlab.com/quoeamaster/alfredo

go 1.17

require gitlab.com/quoeamaster/yalf v0.1.0

require github.com/pelletier/go-toml v1.9.4 // indirect
