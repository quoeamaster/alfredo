// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// Alfredo - a compact framework for concurrency / thread programming in golang.
// Copyright (C) 2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package test

import (
	"fmt"
	"math/rand"
	"sync"
	"testing"
	"time"
)

// helper method to create the title-struct for this test suite.
func prepareTitleStructForCancellableWaitgroupWrapper(functionname string, explain string) (o *titleStruct) {
	o = &titleStruct{
		file:     "cancellable_waitgroup_wrapper_test",
		function: functionname,
		explain:  explain,
	}
	return
}

func Test_cancellable_waitgroup_wrapper_original(t *testing.T) {
	fmt.Println(prepareTitleStructForCancellableWaitgroupWrapper("Test_cancellable_waitgroup_wrapper_original", `A demo on how a waitgroup should work. Take it as a reference.`))
	t.Skip()

	// 1. signals
	_taskQ := make(chan int)
	_signalQ := make(chan int)
	_waitgrp := sync.WaitGroup{}
	_lock := sync.Mutex{}
	_closed := false

	// 2. routine for reading the taskQ and _signalQ
	go func() {
		for {
			select {
			case _id := <-_taskQ:
				/* // not necessary here...
				if _closed {
					return
				}
				*/
				fmt.Printf("handling [%v]...\n", _id)
				_waitgrp.Done()

			case _signal := <-_signalQ:
				fmt.Printf("received signal [%v]\n", _signal)

				_lock.Lock()
				// [lesson] - let the taskQ live or else the just created / pending task(s) will be deleted
				//close(_taskQ)
				close(_signalQ)
				// [lesson] - add a state to prevent the routine(s) to continue running after the stop-signal is received
				_closed = true
				_lock.Unlock()

				return
			}
		} // end -- for(true)
	}()

	// 3. routine to add in tasks
	go func() {
		_tick := time.NewTicker(time.Second * 1)
		for {
			_id := <-_tick.C

			_lock.Lock()
			// [lesson] - check the state before submitting jobs (should be encapsulated within the Push() api)
			if !_closed {
				_taskQ <- int(_id.UnixMilli())
			}
			_lock.Unlock()

			_waitgrp.Add(1)
			fmt.Printf("new task created -> [%v]\n", _id.UnixMilli())
		}
	}()

	// 4. a ticker to stop the taskQ and signalQ around 2+random seconds
	rand.Seed(time.Now().UnixMilli())
	_interval := time.Duration(time.Second * time.Duration(2+rand.Intn(5)))
	_stopTick := time.NewTicker(_interval)
	fmt.Printf("** start to wait for %v seconds...\n", _interval.Seconds())

	<-_stopTick.C
	_signalQ <- 0
	fmt.Printf("#### sent the signal 0\n")

	// 5. wait
	_waitgrp.Wait()
	// [bug?] - if lock and close the _taskQ... somehow would deadlock
	//_lock.Lock()
	//close(_taskQ)
	//_lock.Unlock()
}

// implement the api version of cancellable waitgroup
func Test_cancellable_waitgroup_wrapper_api_1(t *testing.T) {
	fmt.Println(prepareTitleStructForCancellableWaitgroupWrapper("Test_cancellable_waitgroup_wrapper_api_1", `demo on calling the api version`))

	_src := "./data"
	_target := "./zip_data"

	_archiver, e := NewArchiverCancellableWaitgroup(_src, _target)
	if e != nil {
		t.Fatalf("failed to init archiver, reason [%v]", e)
	}
	if e := _archiver.Archive(); e != nil {
		t.Fatalf("failed to archive, reason [%v]", e)
	}
}
