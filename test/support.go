// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// Alfredo - a compact framework for concurrency / thread programming in golang.
// Copyright (C) 2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package test

import (
	"archive/zip"
	"fmt"
	"io/fs"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	a "gitlab.com/quoeamaster/alfredo"
	y "gitlab.com/quoeamaster/yalf"
)

// ServicePollingCancellableRoutineChannel - an availability polling service on a URL/URI.
// It keeps on polling until a stop-signal is received. This is a good example on cancellable repeating tasks running in parallel.
type ServicePollingCancellableRoutineChannel struct {
	// how long to tick the poll.
	TickInterval time.Duration
	// the URL/URI for polling.
	Endpoints []string

	// concurrency controller
	cancellableRoutine *a.RoutineChannelCancellable

	stream y.IStream
}

// create an instance of [ServicePollingCancellableRoutineChannel].
func NewServicePollingCancellableRoutineChannel(
	interval time.Duration,
	endpoints []string) (o *ServicePollingCancellableRoutineChannel, e error) {

	o = new(ServicePollingCancellableRoutineChannel)
	o.TickInterval = interval
	o.Endpoints = endpoints
	// stream
	o.stream = y.NewLogFactory().Config(nil).GetStream(a.StreamStdOut)

	return
}

type ServicePollMeta struct {
	endpoint  string
	timestamp time.Time
}

// SetDelegate - another way to set a delegate for execution.
func (o *ServicePollingCancellableRoutineChannel) SetDelegate(delegate a.RoutineChannelCancellableDelegate) (e error) {
	_c, e := a.NewRoutineChannelCancellable(0, delegate)
	o.cancellableRoutine = _c

	return
}

// Poll - run a continuous poll; could be run within another go-routine to prevent blocking.
func (o *ServicePollingCancellableRoutineChannel) Poll() (e error) {
	// running a ticker per second
	// after 5 seconds; a stop signal would be issued to simulate some Cancel api call
	// all routines would stop at once (possilbe that some scheduled routines would be stopped before able to run)
	if _valid := o.validate(); !_valid {
		e = fmt.Errorf("validation failed, either tick-interval is not set OR the endpoints are missing")
		return
	}
	// kick start the cancellable controller
	o.cancellableRoutine.Run()

	// [note]
	// if it were a REST server; then should start a separate goroutine to run the poll
	_ticker := time.NewTicker(o.TickInterval)
	for {
		_ts := <-_ticker.C
		for _, _endpoint := range o.Endpoints {
			// logging
			// [debug]
			//o.Log(fmt.Sprintf("%v -> %v", _ts.Format(time.RFC3339), _endpoint))
			_meta := ServicePollMeta{
				endpoint:  _endpoint,
				timestamp: _ts,
			}
			o.cancellableRoutine.Push(_meta)
		}
	} // end -- for (endless)
}

// validate - validation purpose.
func (o *ServicePollingCancellableRoutineChannel) validate() (isValid bool) {
	isValid = true
	if o.TickInterval.Milliseconds() <= 0 {
		isValid = false
	} else if len(o.Endpoints) == 0 {
		isValid = false
	}
	return
}

func (o *ServicePollingCancellableRoutineChannel) Stop() {
	o.cancellableRoutine.Cancel()
}

// Execute - the implementation of [RoutineChannelCancellableDelegate]
func (o *ServicePollingCancellableRoutineChannel) Execute(meta interface{}) (e error) {
	// conversion
	if _meta, _ok := meta.(ServicePollMeta); !_ok {
		e = fmt.Errorf("[Execute] failed to convert meta data to ServicePollMeta, %v", _meta)
		return
	} else {
		// run a ping
		_r, e2 := http.NewRequest(http.MethodGet, _meta.endpoint, nil)
		if e2 != nil {
			e = e2
			return
		}
		_res, e2 := http.DefaultClient.Do(_r)
		if e2 != nil {
			e = e2
			return
		}
		// check if the service is 200 or 201
		if _res.StatusCode < 200 || _res.StatusCode > 299 {
			e = fmt.Errorf("[Execute] [%v] is available but status is not 2XX, instead {%v}", _meta.endpoint, _res.StatusCode)
			return
		}
		o.Log(fmt.Sprintf("[%v] PING success [%v] -> %v ", _meta.timestamp.Format(time.RFC3339), _res.StatusCode, _meta.endpoint))
	}
	return
}

// Log - logging a message to the corresponding output.
func (o *ServicePollingCancellableRoutineChannel) Log(msg string) {
	// could be implementation specific, for now, hardcode using fmt.Printf()
	CommonLog(o.stream, msg)
}

// CommonLog - logging msg to the stdout.
func CommonLog(stream y.IStream, msg string) {
	if stream != nil {
		stream.Log(msg)
		/*
			stream.LogByModel(y.LogModel{
				Message:       msg,
				Level:         y.LogLevelInfo,
				NeedTimestamp: true,
			})
		*/
	} else {
		fmt.Printf("[%v] %v\n", time.Now().Format(time.RFC3339), msg)
	}
}

// ------ move archiver for waitgroup example -------

// ArchiverCancellablWaitgroup - a supporting object to handle the zipping of files within a folder.
// Make use of goroutines and channels (cancellable) to parallelize the zipping process.
type ArchiverCancellablWaitgroup struct {
	// reading the files for archive
	Folder string
	// target folder storing the archive
	TargetFolder string

	wgController *a.CancellableWaitgroup

	stream y.IStream
}

// archiveMeta - a meta object for task execution later on.
type archiveMeta struct {
	filepath string
	folder   string
}

// ctor.
func NewArchiverCancellableWaitgroup(folder, targetFolder string) (o *ArchiverCancellablWaitgroup, e error) {
	o = &ArchiverCancellablWaitgroup{
		Folder:       folder,
		TargetFolder: targetFolder,
	}
	o.wgController, e = a.NewCancellableWaitgroup(o)
	// stream
	o.stream = y.NewLogFactory().Config(nil).GetStream(a.StreamStdOut)

	return
}

// Archive - archive the given folder's contents 1 by 1 into their corresponding zip file.
func (o *ArchiverCancellablWaitgroup) Archive() (e error) {
	// start the group
	o.wgController.Run()

	// loop through the given [folder]
	filepath.Walk(o.Folder, func(path string, info fs.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		// archive
		// per file matched, update the [channel] for zipping in parallel
		o.wgController.Push(archiveMeta{
			filepath: path,
			folder:   o.TargetFolder,
		})
		return nil
	})
	// [optional] exhausted the folder, send a stop signal to the [stopChannel] -> could be a o.Stop() function call as well...
	// why optional? since usually you won't need to cancel operation.
	//o.wgController.Cancel()

	// [lesson] - must let the caller run the Wait() at a "suitable" moment. Or else the program won't stop.
	// if necessary, could even delay the wait with Time.sleep(n)...
	o.wgController.Wait()

	return
}

func (o *ArchiverCancellablWaitgroup) Execute(meta interface{}) (e error) {
	if _archiveMeta, ok := meta.(archiveMeta); ok {
		_fname := fmt.Sprintf("%v%v%v.zip",
			_archiveMeta.folder,
			string(os.PathSeparator),
			GetFilenameFromAbsPath(_archiveMeta.filepath))

		CommonLog(o.stream, fmt.Sprintf("src: %v -> archive: %v\n", _archiveMeta.filepath, _fname))
		e = ZipFile([]string{_archiveMeta.filepath}, _fname)
		CommonLog(o.stream, fmt.Sprintf("done archving [%v]", _archiveMeta.filepath))
	}
	return
}

// zipFile - zip the provided [path] contents into a [zipFilename].
func ZipFile(path []string, zipFilename string) (e error) {
	_oFile, e := os.OpenFile(zipFilename, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
	if e != nil {
		return
	}
	_zWriter := zip.NewWriter(_oFile)
	defer _zWriter.Close()

	for _, _srcFile := range path {
		_entryWriter, e2 := _zWriter.Create(GetFilenameFromAbsPath(_srcFile))
		if e2 != nil {
			e = e2
			return
		}
		// read the contents from the _srcFile
		_b, e2 := ioutil.ReadFile(_srcFile)
		if e2 != nil {
			e = e2
			return
		}
		_, e2 = _entryWriter.Write(_b)
		if e2 != nil {
			e = e2
			return
		}
	}
	return
}

// getFilenameFromAbsPath - get the filename without the absolute-path
func GetFilenameFromAbsPath(path string) (file string) {
	file = path
	if _idx := strings.LastIndex(path, string(os.PathSeparator)); _idx != -1 {
		file = path[_idx+1:]
	}
	return
}
