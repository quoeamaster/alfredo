// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// Alfredo - a compact framework for concurrency / thread programming in golang.
// Copyright (C) 2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package test

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
)

// [Run] -> go test -race -v core_test.go routine_channel_wrapper_test.go routine_channel_wrapper_support.go
// -race = race detection which is important for goroutine related code
// -v is for verbose debug to be available

// prepareTitleStruct - helper method to create the title-struct for this test suite.
func prepareTitleStructForRoutineChannelWrapper(functionname string, explain string) (o *titleStruct) {
	o = &titleStruct{
		file:     "routine_channel_wrapper_test",
		function: functionname,
		explain:  explain,
	}
	return
}

// [lesson]
// - the main func / logic must have a endless loop like operation to keep the other routines to be at least running till the last.
// - this approach is like spawning as many tasks as possible BUT there is no quarantee that
//   all started tasks would be finished before the program ends. Hence might have unexpected results.
// - this approach is GREAT if you need to stop the routines suddenly. If not should pick the [workgroup] approach
//   for guaranteed execution of every running routine.
func Test_routine_channel_worker_original(t *testing.T) {
	// [debug] - test on long long explain of the TitleStruct...
	//fmt.Println(prepareTitleStructForRoutineChannelWrapper("Test_routine_channel_worker_original", `Happy new year~ Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Magna eget est lorem ipsum dolor sit amet. Viverra justo nec ultrices dui sapien eget. Diam phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet. Egestas fringilla phasellus faucibus scelerisque. Dui vivamus arcu felis bibendum. In pellentesque massa placerat duis ultricies lacus sed turpis. Rhoncus urna neque viverra justo nec ultrices dui. Hac habitasse platea dictumst vestibulum rhoncus est pellentesque. Posuere lorem ipsum dolor sit amet. Porttitor lacus luctus accumsan tortor posuere ac ut consequat. Sagittis id consectetur purus ut faucibus pulvinar elementum integer enim. Fermentum iaculis eu non diam. Condimentum mattis pellentesque id nibh tortor id aliquet lectus proin. Ac orci phasellus egestas tellus rutrum tellus pellentesque eu. Dictum fusce ut placerat orci. Vel turpis nunc eget lorem dolor. Luctus venenatis lectus magna fringilla urna porttitor rhoncus dolor purus. Tempus quam pellentesque nec nam aliquam sem et tortor. Suspendisse ultrices gravida dictum fusce ut placerat orci nulla pellentesque. Iaculis at erat pellentesque adipiscing commodo elit. Sed risus pretium quam vulputate dignissim suspendisse in. Elementum curabitur vitae nunc sed velit. Ipsum faucibus vitae aliquet nec ullamcorper. Diam vulputate ut pharetra sit. Ut sem viverra aliquet eget sit amet. Hac habitasse platea dictumst quisque sagittis purus sit. Aliquet nec ullamcorper sit amet risus nullam eget felis. Ut tellus elementum sagittis vitae et leo duis ut diam. Rhoncus dolor purus non enim praesent. Tellus rutrum tellus pellentesque eu. Enim neque volutpat ac tincidunt. Maecenas ultricies mi eget mauris pharetra. Scelerisque fermentum dui faucibus in ornare quam viverra. Proin libero nunc consequat interdum varius sit amet. Aliquet eget sit amet tellus cras adipiscing enim eu. Lorem ipsum dolor sit amet consectetur adipiscing elit ut. Vivamus at augue eget arcu dictum varius duis at. Neque convallis a cras semper. Mi tempus imperdiet nulla malesuada pellentesque elit eget gravida. Amet est placerat in egestas erat. Velit sed ullamcorper morbi tincidunt ornare massa eget egestas purus. Eu mi bibendum neque egestas congue quisque. Ultrices eros in cursus turpis. Porttitor rhoncus dolor purus non enim. Semper eget duis at tellus at. Massa massa ultricies mi quis hendrerit. Justo donec enim diam vulputate ut. Purus sit amet volutpat consequat mauris nunc congue nisi vitae. Id interdum velit laoreet id donec ultrices. Ultrices gravida dictum fusce ut placerat orci. At ultrices mi tempus imperdiet nulla malesuada pellentesque elit eget. Id faucibus nisl tincidunt eget nullam non. Orci sagittis eu volutpat odio facilisis mauris sit amet. Lectus proin nibh nisl condimentum id. A scelerisque purus semper eget duis at tellus. Enim sit amet venenatis urna cursus eget nunc. Quis auctor elit sed vulputate mi. Augue mauris augue neque gravida. Pharetra et ultrices neque ornare. Odio morbi quis commodo odio aenean sed. Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique. Vel elit scelerisque mauris pellentesque pulvinar pellentesque. Consequat id porta nibh venenatis cras sed felis. Quisque egestas diam in arcu cursus. Amet tellus cras adipiscing enim eu turpis. Vitae purus faucibus ornare suspendisse sed nisi lacus sed viverra. Eget sit amet tellus cras adipiscing enim eu turpis egestas. Nam libero justo laoreet sit amet cursus sit amet. Curabitur vitae nunc sed velit dignissim sodales ut. Nec sagittis aliquam malesuada bibendum arcu vitae elementum. Faucibus purus in massa tempor nec feugiat. Pharetra pharetra massa massa ultricies mi quis. Urna condimentum mattis pellentesque id nibh. Praesent tristique magna sit amet purus. Pellentesque sit amet porttitor eget dolor morbi non arcu. Auctor elit sed vulputate mi sit amet mauris. Lorem sed risus ultricies tristique nulla aliquet enim tortor.`).String())
	fmt.Println(prepareTitleStructForRoutineChannelWrapper("Test_routine_channel_worker_original", `a demo of non API-ready test for goroutine and channel interactions. Take it as a reference example.`).String())
	t.Skipf("skipped as this method is for demonstration on how to run a cancellable routine in a non api way\n")

	// 0. channels setup
	products := make(chan int) // unbuffered (means could be any length)
	signals := make(chan int)  // a signal channel for stopping the routine(s)

	// another ticker to tick around 5 seconds and stop the channels...
	go func() {
		// [debug]
		// fmt.Printf("## 2. inside the 5 sec tick\n")
		_ticker := time.NewTicker(time.Second * 5)
		// blocks until 5 seconds lapsed and send a stop signal to the signals channel.
		<-_ticker.C
		signals <- 0
	}()

	// a routine to print out incoming timestamps
	go func() {
		// [debug]
		// fmt.Printf("## 3. inside products consumption...\n")
		for _timestamp := range products {
			// apply a random sleep (0 to 3 seconds)
			_ts := _timestamp
			go func() {
				time.Sleep(time.Second * time.Duration(rand.Intn(3)))
				fmt.Printf("handling product [%v]...\n", _ts)
			}()
		}
	}()

	// [debug]
	// run an infinite loop to make sure the program won't stop unless a signal received
	//
	// [lesson]
	// the main func / logic must have a endless loop like operation to keep the other routines to be at least running till the last.
	_ticker := time.NewTicker(time.Second)
	for {
		select {
		case _signal := <-signals:
			if _signal == 0 {
				close(signals)
				close(products)
				// exit the goroutine and stop the _ticker.C consumption
				return
			}
		case _timestamp := <-_ticker.C:
			// [debug]
			fmt.Printf("## 1b. timestamp received {%v}\n", _timestamp.UnixMilli())
			// ticker.C is a <-chan time.Time; hence no matter you need the value or not; need to get it by "<-ticker.C"
			products <- int(_timestamp.UnixMilli())
		}
	}

	// [work-but-not-robust]
	// make sure the above gorountines have at least run for 1 seconds
	// not a robust approach...
	//time.Sleep(time.Second * 7)

	// [not-work]
	// wait till all gorountines done or forever...
	//runtime.Gosched()
}

func Test_ServicePollingCancellableRoutineChannel(t *testing.T) {
	fmt.Println(prepareTitleStructForRoutineChannelWrapper("Test_ServicePollingCancellableRoutineChannel",
		`a demo of the API-ready test for cancellable routine and channel. Reference example.`).String())

	_srv, e := NewServicePollingCancellableRoutineChannel(time.Second, []string{"https://www.google.com/", "https://sg.yahoo.com/"})
	if e != nil {
		t.Fatalf("#0. failed to create the polling-service, %v\n", e)
	}
	// 1. set delegate
	if e := _srv.SetDelegate(_srv); e != nil {
		t.Fatalf("#1. failed to set delegate on polling-service, %v\n", e)
	}
	// 2. poll, needs to be a goroutine since it has a "for (true) {}" loop
	go func() {
		_srv.Poll()
	}()
	// 3. setup another timer to stop at a random second
	rand.Seed(time.Now().UnixMilli())
	_interval := time.Second * time.Duration(3+rand.Intn(10))
	fmt.Printf("stop around a random period of %v seconds...\n", _interval.Seconds())

	_ticker := time.NewTicker(_interval)
	<-_ticker.C

	// 4. stop the polls
	_srv.Stop()
}

// TODO: mimic a scenario to make use of channel and goroutine...
// - a function to loop through a directory and search for files 1 by 1
// - pump in the file path to a channel
// - the receiver of the channel starts to process the zip
// - when the directory is exhausted; will send a "stop" signal to the stop-channel and everything would stop all channels
// - once all routines done (the process / zip channel); the program should end...
