// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// Alfredo - a compact framework for concurrency / thread programming in golang.
// Copyright (C) 2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package alfredo

const (
	// the stop signal
	SignalStop int = 999

	/*
		// the log level for Alfredo library; default is DEBUG
		EnvVarLogLevel string = "AL_LOG_LV"

		LogLevelDebug string = "DEBUG"
		LogLevelInfo  string = "INFO"
		LogLevelWarn  string = "WARN"
		LogLevelError string = "ERROR"
	*/

	StreamStdOut string = "stdout"
)
